const ansi = {};
const reset = `\x1b[0m`;

for (const [key, value] of Object.entries({
	blink: `\x1b[5m`,
	bright: `\x1b[1m`,
	dim: `\x1b[2m`,
	hidden: `\x1b[8m`,
	reverse: `\x1b[7m`,
	underscore: `\x1b[4m`,

	black: `\x1b[30m`,
	blue: `\x1b[34m`,
	cyan: `\x1b[36m`,
	green: `\x1b[32m`,
	magenta: `\x1b[35m`,
	red: `\x1b[31m`,
	white: `\x1b[37m`,
	yellow: `\x1b[33m`,

	bgBlack: `\x1b[40m`,
	bgBlue: `\x1b[44m`,
	bgCyan: `\x1b[46m`,
	bgGreen: `\x1b[42m`,
	bgMagenta: `\x1b[45m`,
	bgRed: `\x1b[41m`,
	bgWhite: `\x1b[47m`,
	bgYellow: `\x1b[43m`
})) {
	ansi[key] = string => `${value}${string}${reset}`;
}

module.exports = { ...ansi };
