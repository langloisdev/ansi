# Contributing Guide

## Styleguide
### Git Messages

- use the present tense
- use the imperative mood
- keep commit messages short and succint
- start your commit message with an appropriate emoji:
	- :art: cleaning/re-organizing code
	- :bug: fixing a bug
	- :arrow_down: downgrading dependencies
	- :arrow_up: upgrading dependencies
	- :white_check_mark: adding tests
	- :shirt: fixing linter errors/warnings
	- :link: CI
	- :zap: performance
	- :notebook: documentation
	- :skull: removing code/file
	- :lock: security
	- :star: feature

### Documentation
We use Markdown for our documentation.

### JavaScript
We use the [Recommended VueJS ESLint rules](https://eslint.vuejs.org/rules/) for our VueJS components as well as the [Standard ESLint rules](https://standardjs.com/) with the exception of using `tabs` instead of `spaces` for indentation.
