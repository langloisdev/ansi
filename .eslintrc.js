const isProduction = () => process.env.NODE_ENV === `production`;

module.exports = {
	env: {
		browser: true,
		es6: true,
		node: true
	},
	extends: [
		`eslint:recommended`
	],
	overrides: [{
		env: { jest: true },
		files: [`*.spec.js`, `./src/**/__mocks__/**/*.js`],
		rules: {
			'no-unused-expressions': 0
		}
	}],
	parserOptions: {
		allowImportExportEverywhere: true,
		ecmaVersion: 2020,
		sourceType: `module`
	},
	root: true,
	rules: {
		indent: [`error`, `tab`],
		'no-console': isProduction() ? `error` : `off`,
		'no-debugger': isProduction() ? `error` : `off`,
		'no-tabs': [`error`, {
			allowIndentationTabs: true
		}],
		quotes: [`error`, `backtick`],
		'require-atomic-updates': `off`,
		semi: [`error`, `always`]
	}
}
