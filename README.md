# ANSI

A JavaScript module to use ANSI escape codes in the terminal.

```js
const { bgWhite, blue, bright, cyan } = require(`@langlois/ansi`);

console.log(cyan(`Hello, world.`));
console.log(bright(blue(`Hello, earth.`)));
console.log(bgWhite(bright(blue(`Hello, earth.`))));
```

## API

The available functions are:

	- blink
	- bright
	- dim
	- hidden
	- reverse
	- underscore

`console.log(underscore('some text'));`

The available colors and background colors are:

	- black
	- blue
	- cyan
	- green
	- magenta
	- red
	- white
	- yellow

	- bgBlack
	- bgBlue
	- bgCyan
	- bgGreen
	- bgMagenta
	- bgRed
	- bgWhite
	- bgYellow

`console.log(blue('some text'));`
`console.log(bgBlue('some text'));`
