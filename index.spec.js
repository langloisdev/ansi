const ansi = require(`./index.js`);

describe(`ANSI module`, () => {
	test(`exists`, () => expect(ansi).toBeDefined());
	test(`is an Object`, () => expect(ansi).toBeInstanceOf(Object));

	test.each(Object.entries({
		blink: `\x1b[5mHello, world.\x1b[0m`,
		bright: `\x1b[1mHello, world.\x1b[0m`,
		dim: `\x1b[2mHello, world.\x1b[0m`,
		hidden: `\x1b[8mHello, world.\x1b[0m`,
		reverse: `\x1b[7mHello, world.\x1b[0m`,
		underscore: `\x1b[4mHello, world.\x1b[0m`,

		bgBlack: `\x1b[40mHello, world.\x1b[0m`,
		bgBlue: `\x1b[44mHello, world.\x1b[0m`,
		bgCyan: `\x1b[46mHello, world.\x1b[0m`,
		bgGreen: `\x1b[42mHello, world.\x1b[0m`,
		bgMagenta: `\x1b[45mHello, world.\x1b[0m`,
		bgRed: `\x1b[41mHello, world.\x1b[0m`,
		bgWhite: `\x1b[47mHello, world.\x1b[0m`,
		bgYellow: `\x1b[43mHello, world.\x1b[0m`,

		black: `\x1b[30mHello, world.\x1b[0m`,
		blue: `\x1b[34mHello, world.\x1b[0m`,
		cyan: `\x1b[36mHello, world.\x1b[0m`,
		green: `\x1b[32mHello, world.\x1b[0m`,
		magenta: `\x1b[35mHello, world.\x1b[0m`,
		red: `\x1b[31mHello, world.\x1b[0m`,
		white: `\x1b[37mHello, world.\x1b[0m`,
		yellow: `\x1b[33mHello, world.\x1b[0m`
	}))('function %s returns "%s"', (func, expected) => expect(ansi[func](`Hello, world.`)).toStrictEqual(expected));
});
